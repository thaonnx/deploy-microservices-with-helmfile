helm install -f values/rediscart-values.yaml rediscart charts/redis

helm install -f values/adservice-values.yaml adservice charts/microservice
helm install -f values/cartservice-values.yaml cartservice charts/microservice
helm install -f values/currencyservice-values.yaml currencyservice charts/microservice
helm install -f values/checkoutservice-values.yaml checkoutservice charts/microservice
helm install -f values/email-service-values.yaml emailservice charts/microservice
helm install -f values/frontend-values.yaml frontendservice charts/microservice
helm install -f values/paymentservice-values.yaml paymentservice charts/microservice
helm install -f values/productcatalog-service-values.yaml productcatalogservice charts/microservice
helm install -f values/recommendation-service-values.yaml recommendationservice charts/microservice
helm install -f values/shippingservice-values.yaml shippingservice charts/microservice